"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");
const Database = use("Database");

//  "name, yds, type, st_y(meta_parent_latlng) as lat, st_x(meta_parent_latlng) as lng, meta_parent_sector, meta_mp_route_id, meta_mp_sector_id ";

/**
 * @swagger
 * definitions:
 *   Climb:
 *     type: object
 *     properties:
 *       name:
 *         type: string
 *         description: Name of the climb/boulder problem.
 *       yds:
 *         type: string
 *         description: YDS grade V scale.
 *       type:
 *         type: object
 *         description: Type of the climb.
 *       fa:
 *         type: string
 *         description: FA information.
 *       lat:
 *         type: number
 *         format: double
 *         description: Approximate latitude of the wall or boulder where this climb is located.
 *       lng:
 *         type: number
 *         format: double
 *         description: Approximate longitude of the wall or boulder where this climb is located.
 *       meta_parent_sector:
 *         type: string
 *         description: Name of the wall or boulder where this climb is located.
 *       meta_mp_route_id:
 *         type: string
 *         description: MountainProject id
 *       meta_mp_sector_id:
 *         type: string
 *         description: MountainProject id of the parent wall.
 * 
 *   SectorSummary:
 *     type: object
 *     properties:
 *       mp_sector_id:
 *         type: string
 *         description: MountainProject ID
 *       sector_name:
 *         type: string
 *         description: Name of this sector/wall/boulder.
 *       lat:
 *         type: number
 *         format: double
 *         description: Approximate latitude.
 *       lng:
 *         type: number
 *         format: double
 *         description: Approximate longitude.
 *       count:
 *         type: number
 *         description: Number of climbs or boulder problems.
 *       mp_ids:
 *         type: string
 *         description: \'|\' delimited list of climb/problem IDs.
 */
class Climb extends Model {}

module.exports = Climb;
