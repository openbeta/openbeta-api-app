"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Utils = require("./Utils");

const Climb = use("App/Models/Climb");
const Database = use("Database");

const STANDARD_COLUMNS =
  "meta_mp_sector_id, meta_parent_sector, st_y(meta_parent_latlng) as lat, st_x(meta_parent_latlng) as lng, count(name), string_agg(meta_mp_route_id,'|') as mp_ids";

class SectorController {
  /**
   * @swagger
   * /geocode/v1/sectors:
   *   get:
   *     tags:
   *     - Sectors
   *     summary: Find sectors near latitude, longitude.
   *     parameters:
   *       - name: latlng
   *         description: A latitude and longitude for which you want to find nearest sectors. The server accepts latitude and longitude up 10 decimal places, and ignores request > 10 dicimal places. The pair should be in `latitude,longitude` format without space.  See example.
   *         in: query
   *         required: false
   *         type: string
   *         example: 36.135626,-115.428135
   *       - name: radius
   *         description: A radius in km to limit your latitude/longitude search.
   *         in: query
   *         required: false
   *         type: integer
   *         minimum: 1
   *         maximum: 100
   *         default: 3
   *     responses:
   *       200:
   *         description: The sector summary.
   *         schema:
   *           $ref: '#/definitions/SectorSummary'
   *
   */
  async index({ params, request, response, view }) {
    const data = request.only(["latlng", "radius"]);

    if (data.latlng) {
      const match = Utils.LATLNG_REGEX.exec(data.latlng);
      if (!match) {
        return [];
      }
      const { lat, long } = match.groups;
      const radius_meters = data.radius ? data.radius * 1000 : 3000;

      var sql = `select ${STANDARD_COLUMNS} from climbs where ST_DWithin(meta_parent_latlng, ST_Point(?,?)::geography,?) group by meta_mp_sector_id, meta_parent_sector, meta_parent_latlng LIMIT 1000`;
      var bindings = [
        +long,
        +lat,
        radius_meters > 100000 ? 100000 : radius_meters,
      ];
      if (data.type) {
        //TODO: filter by types eg trad, sport, etc
        sql = sql + " and true=true";
      }
      const rs = await Database.raw(sql, bindings);

      return rs.rows;
    }
    return [];
  }

  /**
   * @swagger
   * /geocode/v1/sectors/{id}:
   *   get:
   *     tags:
   *     - Sectors
   *     summary: Get a sector summary by ID.  All climbs/boulder problems belong to a sector.  Nested sectors are not supported yet.
   *     parameters:
   *     - in: path
   *       name: id
   *       schema:
   *         type: integer
   *       required: true
   *       description: ID of the sector (wall) to get.
   *       example: 105732162
   *     responses:
   *       200:
   *         description: The sector summary.
   *         schema:
   *           $ref: '#/definitions/SectorSummary'
   *
   */
  async show({ params, request, response, view }) {
    const sql =
      "select meta_mp_sector_id, meta_parent_sector, st_y(meta_parent_latlng) as lat, st_x(meta_parent_latlng) as lng, count(name), string_agg(meta_mp_route_id,'|') as mp_ids from climbs where meta_mp_sector_id=? group by meta_mp_sector_id, meta_parent_sector, meta_parent_latlng";
    const rs = await Database.raw(sql, [params.id]);
    return rs.rows.length === 1 ? rs.rows[0] : {};
  }
}

module.exports = SectorController;
