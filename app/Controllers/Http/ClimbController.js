"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Utils = require("./Utils");

const Climb = use("App/Models/Climb");
const Database = use("Database");

const STANDARD_COLUMNS =
  "name, yds, type, fa, st_y(meta_parent_latlng) as lat, st_x(meta_parent_latlng) as lng, meta_parent_sector, meta_mp_route_id, meta_mp_sector_id ";

class ClimbController {
  /**
   * @swagger
   * /geocode/v1/climbs:
   *   get:
   *     tags:
   *       - Climbs
   *     summary: Find climbs by name, first ascensionists, or by nearest latitude/longitude (reverse geocoding).  The server only supports one type of parameter per search.
   *     parameters:
   *       - name: name
   *         description: A full or partial name of the route or boulder problem.  The server ignores request with name less than 3 characters.
   *         in: query
   *         required: false
   *         type: string
   *         example: Cat in the hat
   *       - name: fa
   *         description: A full or partial name of FA party.  The server ignores request with name less than 3 characters.
   *         in: query
   *         required: false
   *         type: string
   *         example: Joanne Urioste
   *       - name: latlng
   *         description: A latitude and longitude for which you want to find nearest climbs. The server accepts latitude and longitude up 10 decimal places, and ignores request > 10 dicimal places. The pair should be in `latitude,longitude` format without space.  See example.
   *         in: query
   *         required: false
   *         type: string
   *         example: 36.135626,-115.428135
   *       - name: radius
   *         description: A radius in km to limit your latitude/longitude search (to be used with latlng search).
   *         in: query
   *         required: false
   *         type: integer
   *         minimum: 1
   *         maximum: 100
   *     responses:
   *       200:
   *         description: Array of matching climbs.
   *         type: array
   *         items:
   *           $ref: '#/definitions/Climb'
   * /geocode/v1/climbs/{id}:
   *   get:
   *     tags:
   *     - Climbs
   *     summary: Get one or more climb details by MP IDs.
   *     parameters:
   *     - in: path
   *       name: id
   *       schema:
   *         type: string
   *       required: true
   *       description: ID of the climb to get.  Can be a single ID or multiple IDs separated by | character
   *       example: 105732374|118309782
   *     responses:
   *       200:
   *         description: The matching climb wrapped in an array.
   *         type: array
   *         items:
   *           $ref: '#/definitions/Climb'
   */
  async index({ params, request, response, view }) {
    const data = request.only(["name", "latlng", "radius", "type", "fa"]);

    if (data.name && data.name.length > 2) {
      var sql = "";

      var search_str = data.name;

      sql = `select ${STANDARD_COLUMNS} from climbs where name ilike ? LIMIT 10`;

      var rs = await Database.raw(sql, search_str + "%");

      if (rs.rows.length > 0) {
        return rs.rows;
      } else {
        sql = `select ${STANDARD_COLUMNS} from climbs where name_vector @@ plainto_tsquery('english', ?) LIMIT 10`;
      }

      return await Database.raw(sql, search_str);
    }

    if (data.latlng) {
      const match = Utils.LATLNG_REGEX.exec(data.latlng);
      if (!match) {
        return [];
      }
      const { lat, long } = match.groups;
      const radius_metters = data.radius ? data.radius * 1000 : 3000;

      var sql = `select ${STANDARD_COLUMNS} from climbs where ST_DWithin(meta_parent_latlng, ST_Point(?,?)::geography,?) LIMIT 1000`;
      var bindings = [
        +long,
        +lat,
        radius_metters > 100000 ? 100000 : radius_metters,
      ];
      if (data.type) {
        //TODO: filter by types eg trad, sport, etc
        sql = sql + " and true=true";
      }
      const rs = await Database.raw(sql, bindings);

      return rs.rows;
    }

    if (data.fa && data.fa.length > 2) {
      var sql = `select ${STANDARD_COLUMNS} from climbs where fa_vector @@ phraseto_tsquery('simple', ?) LIMIT 1000`;

      const rs = await Database.raw(sql, [data.fa]);

      if (rs.rows.length > 8) {
        return rs.rows;
      }
      sql = `select name, yds, type, fa, lat, lng, meta_parent_sector, meta_mp_route_id, meta_mp_sector_id from (select ${STANDARD_COLUMNS}, word_similarity(fa, ?) as sim from climbs where fa % ? order by sim desc, fa) as X limit 1000`;
      const rs2 = await Database.raw(sql, [data.fa, data.fa]);
      return rs.rows.concat(rs2.rows);
    }

    return await Climb.first();
  }

  /**
   * Render a form to be used for creating a new climb.
   * GET climbs/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new climb.
   * POST climbs
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {}

  /**
   * Display a single climb.
   * GET climbs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    const list = params.id && params.id.split("%7C"); // %7C = '|'
    if (list.length < 1) {
      return [];
    } else {
      return Database.select(
        "name",
        "yds",
        "type",
        "fa",
        "meta_parent_latlng",
        "meta_parent_sector",
        "meta_mp_route_id",
        "meta_mp_sector_id"
      )
        .from("climbs")
        .whereIn("meta_mp_route_id", list);
    }
  }

  /**
   * Render a form to update an existing climb.
   * GET climbs/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update climb details.
   * PUT or PATCH climbs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a climb with id.
   * DELETE climbs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}
}

const csv_to_dict = (csv) => {
  const zz = csv.split(",");
};
module.exports = ClimbController;
