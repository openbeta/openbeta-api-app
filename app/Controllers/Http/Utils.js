"use strict";

module.exports = {
  LATLNG_REGEX: RegExp(
    /^(?<lat>(-?(90|(\d|[1-8]\d)(\.\d{1,10}){0,1})))\,{1}(?<long>(-?(180|(\d|\d\d|1[0-7]\d)(\.\d{1,10}){0,1})))$/
  ),
};
