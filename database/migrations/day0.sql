-- run this after initial migration script

psql -U openbeta -W

-- generate lexemes
update climbs set name_vector = to_tsvector('english', name);

-- create index
create index idx_name_vector ON climbs USING gin(name_vector);

-- new FA lexemes
alter table climbs add column fa_vector tsvector;
update climbs set fa_vector = to_tsvector('simple', fa);
create index idx_fa_vector ON climbs USING gin(fa_vector);

-- Enable trigram extension
CREATE EXTENSION pg_trgm;
CREATE INDEX trgm_fa_idx ON climbs USING GIN (fa gin_trgm_ops);