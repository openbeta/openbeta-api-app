"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class ClimbsSchema extends Schema {
  up() {
    this.create("climbs", (table) => {
      table.increments();
      table.string("name", 300).notNullable();
      table.string("yds", 8)
      table.integer("yds_w").notNullable();
      table.string("safety", 5);
      table.jsonb("type").notNullable();
      table.string("fa", 200);
      table.specificType("description", "varchar[]");
      table.specificType("location", "varchar[]");
      table.specificType("protection", "varchar[]");
      table.integer("meta_left_right_seq").notNullable();
      table.specificType("meta_parent_latlng", "geometry(point, 4326)");
      table.string("meta_parent_sector", 200);
      table.string("meta_mp_route_id", 15).notNullable();;
      table.string("meta_mp_sector_id", 15).notNullable();;
      table.timestamp("created_at").notNullable();
      table.timestamp("updated_at").notNullable();
      table.specificType("name_vector", "tsvector");
      table.index(['name']);
      table.index(['meta_mp_route_id']);
      table.index(['meta_parent_latlng'], 'meta_parent_latlng', 'GIST');
      table.unique(['name', 'meta_mp_route_id']);

    });
  }

  down() {
    this.drop("climbs");
  }
}

module.exports = ClimbsSchema;
