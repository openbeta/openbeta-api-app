"use strict";

/*
|--------------------------------------------------------------------------
| ClimbSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");
const Database = use("Database");

class ClimbSeeder {
  async run() {
    //const climbs = await Database.table("climbs");
    //console.log(climbs);
    await Factory.model("App/Models/Climb").createMany(3);
  }
}

module.exports = ClimbSeeder;
