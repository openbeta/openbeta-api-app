'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

Factory.blueprint('App/Models/Climb', (faker) => {
  return {
    name: faker.sentence({ words: 8 }),
    yds: "5.9",
    yds_w: "90",
    safety: "PG13",
    type: {"trad": true},
    fa: faker.name() + " " + faker.year({min: 1968, max: 2015}),
    description: [faker.sentence(), faker.sentence(), faker.sentence()],
    location: [faker.sentence()],
    protection: [faker.sentence()],
    meta_parent_latlng: 'Point(41.15478 105.37467)',
    meta_parent_sector: faker.sentence({ words: 5 }),
    meta_left_right_seq: "2",
    meta_mp_route_id: "12345678",
    meta_mp_sector_id: "123456"
  }
})

