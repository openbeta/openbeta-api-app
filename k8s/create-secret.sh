# Go to Gitlab project settings -> Repository -> Deploy Tokens
# Create one with name: gitlab-deploy-token
# $DOCKER_USER AND $DOCKER_PASSWORD are user name and token (not shown once)

DOCKER_USER=gitlab+deploy-token-269398

kubectl config set-context --current --namespace=openbeta

kubectl create secret docker-registry registry-openbeta-api \
--docker-server=https://registry.gitlab.com \
--docker-username=$DOCKER_USER \
--docker-password=$DOCKER_PASSWORD \
--docker-email=viet2046@gmail.com
