FROM node:14-slim

ENV APP_DIR=/opt/apiserver
WORKDIR ${APP_DIR}

RUN mkdir -p ${APP_DIR}

EXPOSE 3333

COPY . ./

RUN yarn install  --no-progress && yarn global add @adonisjs/cli

ENV NODE_ENV=production

CMD [ "adonis", "serve" ]
