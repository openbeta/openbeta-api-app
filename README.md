# OpenBeta API server

OpenBeta API server is a Node.js application that allows you to programmatically access information about climbing routes, GPS coordinates, grades, first ascensionists (FA) and more.

To learn more about the API, visit the [online documentation](https://climb-api.openbeta.io/docs).

## Setup a development environment

1. The database

We suggest running PostGIS database in a Docker container.  Setup [instructions](./hacks).

2.  The API server (Node.js 12 or later)

```
# Install dependencies
yarn install

# Start the API server
adonis serve --dev
```

# Contribution
Feedback/pull-requests are welcome and appreciated.