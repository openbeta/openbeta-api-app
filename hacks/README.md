# How to setup PostGIS database for local development

Launch a PostGIS container

```
# make sure to run this command in `hacks` dir
cd hacks
docker run --name openbeta-postgis -p 5432:5432 \
   -e POSTGRES_PASSWORD=tac0s123 \
   -e POSTGRES_USER=openbeta -e POSTGRES_DB=openbeta \
   -e POSTGRES_HOST_AUTH_METHOD=md5 \
   -v $(pwd)/day0-data:/day0-data postgis/postgis:13-3.1-alpine
```

Load data into the database

```
docker exec -it openbeta-postgis pg_restore -U openbeta -d openbeta /day0-data/openbeta.dump
```
